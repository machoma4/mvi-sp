import os

import torch
from torch.nn.functional import cosine_similarity
from torch.utils.data import DataLoader
from torchvision import models, transforms, datasets

TRAINED_MODEL = 'resnet50-untrained-adam-epoch-final.pth'
BATCH_SIZE = 50

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model = models.resnet50()
model.load_state_dict(torch.load(os.path.join('models', TRAINED_MODEL)))
model.eval()
model = model.to(device)

inputTransformations = transforms.Compose([
    # Preprocessing belongs here
    transforms.Resize((256, 128)),  # Needed by pretrained models - upscale image
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  # Needed by pretrained models
])

testImages = torch.FloatTensor()
testImagesLabels = torch.LongTensor()

if not os.path.isfile(TRAINED_MODEL + '_testing-tensors.pt'):
    # Predict testing images
    dataset = datasets.ImageFolder(os.path.join('prepared-dataset', 'test'), inputTransformations)
    loader = DataLoader(dataset, batch_size=BATCH_SIZE,
                        shuffle=False)  # Torch can handle only batches, so use loader to batch samples

    for inputs, labels in loader:
        # Send data to GPU (if GPU is available)
        inputs = inputs.to(device)
        labels = labels.to(device)

        predictions = model(inputs)
        testImages = torch.cat((testImages, predictions.data.cpu()))
        testImagesLabels = torch.cat((testImagesLabels, labels.data.cpu()))

    torch.save(testImages, TRAINED_MODEL + '_testing-tensors.pt')
    torch.save(testImagesLabels, TRAINED_MODEL + '_testing-labels.pt')
else:
    print('Loaded previously calculated tensors')
    testImages = torch.load(TRAINED_MODEL + '_testing-tensors.pt')
    testImagesLabels = torch.load(TRAINED_MODEL + '_testing-labels.pt')

# Evaluate query images
dataset = datasets.ImageFolder(os.path.join('prepared-dataset', 'query'), inputTransformations)
loader = DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=False)  # Torch can handle only batches, so use loader to batch samples

correct = 0
samples = 0
for inputs, labels in loader:
    # Send data to GPU (if GPU is available)
    inputs = inputs.to(device)
    labels = labels.to(device)

    predictions = model(inputs)
    for p, l in zip(predictions.data.cpu(), labels.data.cpu()):
        dist = cosine_similarity(testImages, p.reshape(1, -1))

        if bool(l == testImagesLabels[torch.argmax(dist)]):
            correct += 1

        samples += 1

print("Testing accuracy: {}".format( float(correct) / float(samples)))