# Person Re-ID

## Příprava dat
1) Stáhnout data z adresy http://188.138.127.15:81/Datasets/Market-1501-v15.09.15.zip
2) Upravit ve skriptu `prepare-data.py` cestu ke staženému datasetu
3) spustit `prepare-data.py`

## Trénování
1) Nainstalovat závislosti v souboru `requirements.txt`
2) Upravit ve skriptu `main.py` parametry (velikost dávky, model, učící algoritmy, cestu k uložení modelů).
3) spustit `main.py`

## Testování
1) Upravit ve skriptu `test.py` parametry (velikost dávky, model).
2) spustit `test.py`