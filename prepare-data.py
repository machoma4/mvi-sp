import os
from shutil import copyfile
from shutil import rmtree

DESTINATION = './prepared-dataset'
SOURCE = './Market'

if not os.path.exists(DESTINATION):
    os.mkdir(DESTINATION)

phaseDirectoryMap = {
    'train': 'bounding_box_train',
    'test': 'bounding_box_test',
    'query': 'query',
}

for phase in ['train', 'test', 'query']:
    if not os.path.exists(os.path.join(DESTINATION, phase)):
        os.mkdir(os.path.join(DESTINATION, phase))

    for root, dirs, files in os.walk(os.path.join(SOURCE, phaseDirectoryMap[phase]), topdown=False):
        for name in files:
            if (name.endswith('.jpg')):
                className = name.split('_')[0]

                # create directory for class if it does not exist
                if not os.path.exists(os.path.join(DESTINATION, phase, className)):
                    os.mkdir(os.path.join(DESTINATION, phase, className))

                copyfile(os.path.join(root, name), os.path.join(DESTINATION, phase, className, name))


# This will not work for pytorch dataloader
rmtree(DESTINATION + '/test/0000')
rmtree(DESTINATION + '/test/-1')