import os

import torch
from torch.nn.functional import cosine_similarity
from torch.utils.data import DataLoader
from torchvision import models, transforms, datasets
import pickle

TRAINED_MODEL = 'resnet50-untrained-adam-epoch-final.pth'
BATCH_SIZE = 50
THRESHOLD = 0.97
#THRESHOLD = 0.90
#THRESHOLD = 0.97

class ImageFolderWithPaths(datasets.ImageFolder):
    """Custom dataset that includes image file paths. Extends torchvision.datasets.ImageFolder
    https://gist.github.com/andrewjong/6b02ff237533b3b2c554701fb53d5c4d
    """

    # override the __getitem__ method. this is the method that dataloader calls
    def __getitem__(self, index):
        # this is what ImageFolder normally returns
        original_tuple = super(ImageFolderWithPaths, self).__getitem__(index)
        # the image file path
        path = self.imgs[index][0]
        # make a new tuple that includes original and the path
        tuple_with_path = (original_tuple + (path,))
        return tuple_with_path



device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model = models.resnet50()
model.load_state_dict(torch.load(os.path.join('models', TRAINED_MODEL)))
model.eval()
model = model.to(device)

inputTransformations = transforms.Compose([
    # Preprocessing belongs here
    transforms.Resize((256, 128)),  # Needed by pretrained models - upscale image
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  # Needed by pretrained models
])

testImages = torch.FloatTensor()
testImagesLabels = torch.LongTensor()
testImagesFileNames = []

if not os.path.isfile(TRAINED_MODEL + '_testing-tensors.pt'):
    # Predict testing images
    #dataset = datasets.ImageFolder(os.path.join('prepared-dataset', 'test'), inputTransformations)
    dataset = ImageFolderWithPaths(os.path.join('prepared-dataset', 'test'), inputTransformations)
    loader = DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=False)  # Torch can handle only batches, so use loader to batch samples

    for i, (inputs, labels, path) in enumerate(loader, 0):
        # Send data to GPU (if GPU is available)
        inputs = inputs.to(device)
        labels = labels.to(device)

        predictions = model(inputs)
        testImages = torch.cat((testImages, predictions.data.cpu()))
        testImagesLabels = torch.cat((testImagesLabels, labels.data.cpu()))
        testImagesFileNames.extend(path)

    torch.save(testImages, TRAINED_MODEL + '_testing-tensors.pt')
    torch.save(testImagesLabels, TRAINED_MODEL + '_testing-labels.pt')
    file = open(TRAINED_MODEL + '_file_names.pickle', 'wb')
    pickle.dump(testImagesFileNames, file)
    file.close()

else:
    print('Loaded previously calculated tensors')
    testImages = torch.load(TRAINED_MODEL + '_testing-tensors.pt')
    testImagesLabels = torch.load(TRAINED_MODEL + '_testing-labels.pt')
    file = open(TRAINED_MODEL + '_file_names.pickle', 'rb')
    testImagesFileNames = pickle.load(file)
    file.close()

# Evaluate query images
dataset = ImageFolderWithPaths(os.path.join('prepared-dataset', 'query'), inputTransformations)
loader = DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True)  # Torch can handle only batches, so use loader to batch samples

correct = 0
samples = 0
for i, (inputs, labels, path) in enumerate(loader, 0):

    # Send data to GPU (if GPU is available)
    inputs = inputs.to(device)
    labels = labels.to(device)

    predictions = model(inputs)
    for p, l, fn in zip(predictions.data.cpu(), labels.data.cpu(), path):
        print("Query image: {}".format(fn))
        dist = cosine_similarity(testImages, p.reshape(1, -1))


        if bool(l == testImagesLabels[torch.argmax(dist)]):
            correct += 1

        print(torch.max(dist))
        for index, (src) in enumerate(dist, 0):
            if src >= THRESHOLD:
                print("    {}".format(testImagesFileNames[index]))

        samples += 1
    break

print("Testing accuracy: {}".format( float(correct) / float(samples)))