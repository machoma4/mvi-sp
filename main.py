import os
import time

from torch import optim
from torch.nn import init
from torch.utils.data import DataLoader
from torchvision import models, datasets, transforms
import torch
import torch.nn as nn
import torch.nn.functional as F

#torch.set_default_tensor_type(torch.cuda.FloatTensor)

EPOCHS = 50
BATCH_SIZE = 100

def train_model(model, dataset):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = models.resnet50(pretrained=False)
    #model = models.resnet50(pretrained=True)
    # Send model to GPU
    model = model.to(device)
    model.train(True) # We are going to train it, needed by pretrained model

    dataset_length = len(dataset)

    criterion = nn.CrossEntropyLoss()
    #optimizer = optim.SGD(model.parameters(), lr=0.02)
    optimizer = optim.Adam(model.parameters(), lr=0.0001)

    for epoch in range(EPOCHS):
        start_time = time.process_time()
        running_corrects = 0.0
        running_loss = 0.0
        for inputs, labels in dataset:
            # Send data to GPU (if GPU is available)
            inputs = inputs.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()
            predictions = model(inputs)
            running_corrects += float(torch.sum(torch.argmax(predictions.data, 1) == labels.data))

            loss = criterion(predictions, labels)
            running_loss += loss.item() * inputs.shape[0]
            loss.backward()
            optimizer.step()

        epoch_acc = running_corrects / dataset_length
        epoch_loss = running_loss / dataset_length

        print('Epoch {}: Loss: {:.4f} Acc: {:.4f}, took {}s'.format(
            epoch, epoch_loss, epoch_acc, time.process_time() - start_time))

        if epoch % 10 == 9:
            torch.save(model.state_dict(), os.path.join('models', 'resnet50-untrained-adam-epoch-{}.pth'.format(epoch)))

    torch.save(model.state_dict(), os.path.join('models', 'resnet50-untrained-adam-epoch-{}.pth'.format('final')))

inputTransformations = transforms.Compose([
    # Preprocessing belongs here
    transforms.Resize((256, 128)),  # Needed by pretrained models - upscale image
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  # Needed by pretrained models
])

dataset = datasets.ImageFolder('./prepared-dataset/train', inputTransformations)
loader = DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True)  # Torch can handle only batches, so use loader to batch samples

train_model(False, loader)
